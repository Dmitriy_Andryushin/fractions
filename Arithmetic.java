import java.util.Scanner;
/**
 * Это демокласс он создан для показа работы и выполнения
 * некоторых задач по типу : сложение , вычитания , деление
 * умножения, сокращение дроби
 */
 
public class Arithmetic {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Напишите выражение так, чтобы знак операчии окружал пробел, пример:3/7 + 4/7");
        String expression = scanner.nextLine();
        String[] symbols = expression.split(" ");
        String[] op1 = symbols[0].split("/");
        String[] op2 = symbols[2].split("/");
        Fraction f1 = new Fraction(Integer.valueOf(op1[0]),Integer.valueOf(op1[1]));
        Fraction f2 = new Fraction(Integer.valueOf(op2[0]),Integer.valueOf(op2[1]));
        switch (symbols[1]){
            case "+":
                Fraction.add(f1,f2);
                break;
            case "-":
                Fraction.subtraction(f1,f2);
                break;
            case "*":
                Fraction.multiplication(f1,f2);
                break;
            case "/":
                Fraction.division(f1,f2);
                break;
            default:
                System.out.println("Похоже вы ввели некорректный знак операции");
                break;

        }
    }
}

