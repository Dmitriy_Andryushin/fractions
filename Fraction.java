/**
 * В данном классе происходят все вычисления
 * Данный класс умеет считать : сумму , разность
 * произведение , частное , сокращение дроби
 *
 */
 
public class Fraction {
    private int denominator;
    private int numerator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
        this(1, 1);
    }

    public static void add(Fraction f1, Fraction f2) {
        if (f1.denominator == f2.denominator) {
            f1.numerator = f1.numerator + f2.numerator;
        } else if (f1.denominator != f2.denominator) {
            f1.numerator = (f1.numerator * f2.denominator) + (f2.numerator * f1.denominator);
            f1.denominator = f1.denominator * f2.denominator;
        }
        reductionOfFraction(f1);
        fractionConclusion(f1);
    }

    public static void subtraction(Fraction f1, Fraction f2) {
        if (f1.denominator == f2.denominator) {
            f1.numerator = f1.numerator - f2.numerator;
        } else if (f1.denominator != f2.denominator) {
            f1.numerator = (f1.numerator * f2.denominator) - (f2.numerator * f1.denominator);
            f1.denominator = f1.denominator * f2.denominator;
        }
        reductionOfFraction(f1);
        fractionConclusion(f1);
    }

    public static void multiplication(Fraction f1 , Fraction f2){
        f1.numerator= f1.numerator * f2.numerator;
        f1.denominator= f1.denominator * f2.denominator;
        reductionOfFraction(f1);
        fractionConclusion(f1);
    }
    public static void division(Fraction f1, Fraction f2){
        f1.numerator= f1.numerator * f2.denominator;
        f1.denominator=f1.denominator * f2.numerator;
        reductionOfFraction(f1);
        fractionConclusion(f1);
    }

    public static void fractionConclusion(Fraction f1) {
        System.out.printf("Результат выражения равен %d/%d %n", f1.numerator, f1.denominator);
    }
    public static  void reductionOfFraction(Fraction f1){
        int min = Math.abs(f1.numerator);
        int max = f1.denominator;
        for(int i = min; i > 1; i--){
            if((min%i==0) &&(max%i==0)){
                f1.numerator=f1.numerator/i;
                f1.denominator=f1.denominator/i;
            }
        }
    }
}
